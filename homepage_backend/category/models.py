#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):

    # ID of the Category
    id = models.AutoField(primary_key=True)

    # Name of the Category
    name = models.CharField(_("Bezeichnung"), max_length=50)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("category:detail", kwargs={"id": self.id})
