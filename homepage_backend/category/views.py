from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from .models import Category


class CategoryDetailView(LoginRequiredMixin, DetailView):
    model = Category
    # These next two lines tell the view to index lookups by title
    slug_field = "name"
    slug_url_kwarg = "name"


class CategoryRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("category:detail", kwargs={"id": self.request.category.id})


class CategoryUpdateView(LoginRequiredMixin, UpdateView):

    fields = ["name"]

    # we already imported User in the view code above, remember?
    model = Category

    # send the user back to their own page after a successful update

    def get_success_url(self):
        return reverse("category:detail", kwargs={"id": self.request.category.id})

    def get_object(self):
        # Only get the User record for the user making the request
        return Category.objects.get(id=self.request.category.id)


class CategoryListView(LoginRequiredMixin, ListView):
    model = Category
    # These next two lines tell the view to index lookups by title
    slug_field = "name"
    slug_url_kwarg = "name"
