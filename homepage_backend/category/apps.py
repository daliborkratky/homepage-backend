from django.apps import AppConfig


class CategoryConfig(AppConfig):
    name = "homepage_backend.category"
    verbose_name = "Category"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import category.signals  # noqa F401
        except ImportError:
            pass
