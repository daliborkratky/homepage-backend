from django.urls import path

from . import views

app_name = "category"
urlpatterns = [
    path("", view=views.CategoryListView.as_view(), name="list"),
    path("~redirect/", view=views.CategoryRedirectView.as_view(), name="redirect"),
    path("~update/", view=views.CategoryUpdateView.as_view(), name="update"),
    path(
        "<str:title>",
        view=views.CategoryDetailView.as_view(),
        name="detail",
    ),
]
