from rest_framework.views import APIView
from django.http import JsonResponse

from .models import UserRequest


class UserRequestJSONView(APIView):

    def post(self, request):
        try:
            output = UserRequest.objects.create(
                name=request.data.get('name'),
                email=request.data.get('email'),
                phone=request.data.get('phone'),
                sales=request.data.get('sales'),
                leads=request.data.get('leads'),
                popularity=request.data.get('popularity'),
                other=request.data.get('other')
            )
            return JsonResponse(status=400, data={"request": request.data})
        except:
            return JsonResponse(status=500, data={"Error": "500", "Message": "Bad Request"})

