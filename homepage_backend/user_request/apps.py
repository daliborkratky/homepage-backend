from django.apps import AppConfig


class UserRequestConfig(AppConfig):
    name = "homepage_backend.user_request"
    verbose_name = "User_request"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import user_request.signals  # noqa F401
        except ImportError:
            pass
