from django.urls import path

from . import views

app_name = "user_request"
urlpatterns = [
    path("", view=views.UserRequestJSONView.as_view(), name="list"),
]
