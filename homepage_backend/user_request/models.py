#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from django.contrib.postgres.fields import JSONField


class UserRequest(models.Model):

    # ID of the From
    id = models.AutoField(primary_key=True)

    # Name of the User
    name = models.CharField(_("Name"), max_length=50)

    # Email of the User
    email = models.CharField(_("Email"), max_length=50)

    # Phone of the User
    phone = models.CharField(_("Phonenumber"), max_length=15)

    # Sales the User wants
    sales = models.IntegerField(_("Umsatz"))

    # Leads the User wants
    leads = models.IntegerField(_("Leads"))

    # Popularity the User wants to gain
    popularity = JSONField()

    # Other Features the User wants
    other = models.CharField(_("Other Needs"), max_length=100, default=None)

    def __str__(self):
        return self.campaign_name

    def get_absolute_url(self):
        return reverse("campaign_info:detail", kwargs={"id": self.id})
