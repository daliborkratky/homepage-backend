from django.urls import path

from . import views

app_name = "Partner"
urlpatterns = [
    path("", view=views.PartnerListView.as_view(), name="list"),
    path("~redirect/", view=views.PartnerRedirectView.as_view(), name="redirect"),
    path("~update/", view=views.PartnerUpdateView.as_view(), name="update"),
    path(
        "<str:title>",
        view=views.PartnerDetailView.as_view(),
        name="detail",
    ),
]
