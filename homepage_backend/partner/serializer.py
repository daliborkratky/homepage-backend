#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .models import Partner

def serialize_partner(partner: Partner):
    return {
        'name': partner.name,
        'image': partner.image.url
    }
