from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class Partner(models.Model):

    # ID of the Partner
    id = models.AutoField(primary_key=True)

    # Name of the Partner
    name = models.CharField(_("Name of Partner"), max_length=20)

    # Image of the Partner
    image = models.ImageField(
        _("Image of Partner"),
        upload_to='assets/img/partner/',
        default='assets/img/partner/no-img.jpg'
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("partner:detail", kwargs={"id": self.id})
