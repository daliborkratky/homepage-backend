from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from .models import Partner


class PartnerDetailView(LoginRequiredMixin, DetailView):
    model = Partner
    # These next two lines tell the view to index lookups by title
    slug_field = "name"
    slug_url_kwarg = "name"


class PartnerRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("partner:detail", kwargs={"id": self.request.partner.id})


class PartnerUpdateView(LoginRequiredMixin, UpdateView):

    fields = ["name"]

    # we already imported User in the view code above, remember?
    model = Partner

    # send the user back to their own page after a successful update

    def get_success_url(self):
        return reverse("partner:detail", kwargs={"id": self.request.partner.id})

    def get_object(self):
        # Only get the User record for the user making the request
        return Partner.objects.get(id=self.request.Partner.id)


class PartnerListView(LoginRequiredMixin, ListView):
    model = Partner
    # These next two lines tell the view to index lookups by title
    slug_field = "name"
    slug_url_kwarg = "name"
