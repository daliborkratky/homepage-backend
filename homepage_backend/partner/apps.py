from django.apps import AppConfig


class PartnerConfig(AppConfig):
    name = "homepage_backend.partner"
    verbose_name = "partner"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import partner.signals  # noqa F401
        except ImportError:
            pass
