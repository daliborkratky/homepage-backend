from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from .models import CampaignInfo


class CampaignInfoDetailView(LoginRequiredMixin, DetailView):
    model = CampaignInfo
    # These next two lines tell the view to index lookups by title
    slug_field = "campaign_name"
    slug_url_kwarg = "campaign_name"


class CampaignInfoRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("campaign_info:detail", kwargs={"id": self.request.campaign_info.id})


class CampaignInfoUpdateView(LoginRequiredMixin, UpdateView):

    fields = ["campaign_name"]

    # we already imported User in the view code above, remember?
    model = CampaignInfo

    # send the user back to their own page after a successful update

    def get_success_url(self):
        return reverse("campaign_info:detail", kwargs={"id": self.request.campaign_info.id})

    def get_object(self):
        # Only get the User record for the user making the request
        return CampaignInfo.objects.get(id=self.request.campaign_info.id)


class CampaignInfoListView(LoginRequiredMixin, ListView):
    model = CampaignInfo
    # These next two lines tell the view to index lookups by title
    slug_field = "campaign_name"
    slug_url_kwarg = "campaign_name"
