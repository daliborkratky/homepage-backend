#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .models import CampaignInfo

def serialize_campaign_info(campaign_info: CampaignInfo):
    return {
        'access': campaign_info.access,
        'couponing': campaign_info.couponing,
        'daily_users': campaign_info.daily_users,
        'running_time': campaign_info.running_time,
        'selling_points': campaign_info.selling_points,
        'other_info': campaign_info.other_info if campaign_info.other_info is not None else None,
        'digital_signage': 'Digital Signage' if campaign_info.digital_signage else None,
        'dashboard': 'Persönliches Dashboard' if campaign_info.dashboard else None
    }
