from django.apps import AppConfig


class CampaignInfoConfig(AppConfig):
    name = "homepage_backend.campaign_info"
    verbose_name = "CampaignInfo"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import campaign_info.signals  # noqa F401
        except ImportError:
            pass
