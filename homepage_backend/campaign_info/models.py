#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class CampaignInfo(models.Model):

    # ID of the Campaign info
    id = models.AutoField(primary_key=True)

    # Name of the Campaign
    campaign_name = models.CharField(_("Für welche Campaigne ist es?"), max_length=100)

    # The access for the campaign info
    ACCESS_CHOICES = (('online', 'online'), ('off- und online', 'off- und online'), ('offline', 'offline'))
    access = models.CharField(_("Campaign access"), max_length=50, choices=ACCESS_CHOICES)

    # Coupon Handling of the Campaign
    COUPONING_CHOICES = (
        ('physisches in-hand Couponing', 'physisches in-hand Couponing'),
        ('digitales Couponing', 'digitales Couponing'),
        ('', None)
    )
    couponing = models.CharField(_("Campaign Couponing"), max_length=50, choices=COUPONING_CHOICES)

    # Daily Users of the Campaign
    daily_users = models.CharField(_("Daily Users of Campaign"), max_length=50, default="xxx Kunden täglich")

    # Total Running Time of the Campaign in days
    running_time = models.CharField(_("Running Time of Campaign"), max_length=50, default="xxx Tage Laufzeit")

    # Selling Points for the Campaign
    selling_points = models.CharField(_("Selling Points of Campaign"), max_length=50, default="xxx Verkaufsstellen")

    # Other Information of the Campaign
    other_info = models.CharField(_("Other Information of Camoaign"), max_length=50)

    # Digital Signage for Campaign
    digital_signage = models.BooleanField(_("Digital Signage"))

    # Dashboard for the Campaign
    dashboard = models.BooleanField(_("Persönliches Dashboard"))

    def __str__(self):
        return self.campaign_name

    def get_absolute_url(self):
        return reverse("campaign_info:detail", kwargs={"id": self.id})
