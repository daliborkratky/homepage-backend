from .models import Campaign
from homepage_backend.campaign_info.serializer import serialize_campaign_info
from homepage_backend.category.serializer import serialize_category
from homepage_backend.partner.serializer import serialize_partner

def serialize_campaign(campaign: Campaign):
    return {
        'title': campaign.title,
        'short_desc': campaign.short_desc,
        'description': campaign.description,
        'start_date': campaign.start_date,
        'end_date': campaign.end_date,
        'all_year': campaign.all_year,
        'quartal': campaign.quartal,
        'campaign_info': serialize_campaign_info(campaign.campaign_info),
        'category': [serialize_category(x) for x in campaign.category.all()],
        'partners': [serialize_partner(x) for x in campaign.partners.all()],
        'overview': campaign.overview.url,
        'product_image': campaign.product_image.url
    }
