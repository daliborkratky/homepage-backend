#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from homepage_backend.campaign_info.models import CampaignInfo
from homepage_backend.category.models import Category
from homepage_backend.partner.models import Partner


class Campaign(models.Model):

    # ID of the campaign
    id = models.AutoField(primary_key=True)

    # The name of the campaign
    title = models.CharField(_("Title of Campaign"), blank=True, max_length=50)

    # The summary before the long description of the campaign
    short_desc = models.TextField(_("Summary of Campaign"), blank=False, max_length=500)

    # The description of the campaign
    description = models.TextField(_("Description of Campaign"), blank=False, max_length=1000)

    # Start date of the campaign
    start_date = models.DateTimeField()

    # End date of the campaign
    end_date = models.DateTimeField()

    # Campaign has no start or end date, it goes all year
    all_year = models.BooleanField(default=False)

    # Quartal, defying in which Quartal the campaign will be hold
    quartal = models.CharField(_("Quartal of Campaign"), blank=False, max_length=50)

    # The benefits of the Campaign
    campaign_info = models.ForeignKey(CampaignInfo, null=True, on_delete=models.CASCADE, default=None)

    # Category of the Campaign
    category = models.ManyToManyField(Category, default=None)

    # The partner of the Campaign
    partners = models.ManyToManyField(Partner, default=None)

    # Overview image for the Campaign
    overview = models.ImageField()

    # Product_image image for the Campaign
    product_image = models.ImageField()

    # PDF of the Campaign
    pdf = models.FileField(upload_to="PDF/", default=None)

    # is the campaign visible
    visible = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("campaign:detail", kwargs={"id": self.id})
