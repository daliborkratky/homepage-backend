from django.apps import AppConfig


class CampaignConfig(AppConfig):
    name = "homepage_backend.campaign"
    verbose_name = "Campaign"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        try:
            import campaign.signals  # noqa F401
        except ImportError:
            pass
