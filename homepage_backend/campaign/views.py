from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from rest_framework.views import APIView
from django.core import serializers
from django.http import JsonResponse
from .serializer import serialize_campaign

from .models import Campaign


class CampaignDetailView(LoginRequiredMixin, DetailView):
    model = Campaign
    # These next two lines tell the view to index lookups by title
    slug_field = "title"
    slug_url_kwarg = "title"


class CampaignRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("campaign:detail", kwargs={"id": self.request.campaign.id})


class CampaignUpdateView(LoginRequiredMixin, UpdateView):

    fields = ["title"]

    # we already imported User in the view code above, remember?
    model = Campaign

    # send the user back to their own page after a successful update

    def get_success_url(self):
        return reverse("campaign:detail", kwargs={"id": self.request.campaign.id})

    def get_object(self):
        # Only get the User record for the user making the request
        return Campaign.objects.get(id=self.request.campaign.id)


class CampaignListView(LoginRequiredMixin, ListView):
    model = Campaign
    # These next two lines tell the view to index lookups by title
    slug_field = "title"
    slug_url_kwarg = "title"


class CampaignJSONView(APIView):

    def get(self, request):
        return JsonResponse([serialize_campaign(item) for item in Campaign.objects.all()], safe=False)
