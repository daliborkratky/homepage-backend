from django.urls import path

from . import views

app_name = "campaign"
urlpatterns = [
    path("", view=views.CampaignJSONView.as_view(), name="list"),
]
